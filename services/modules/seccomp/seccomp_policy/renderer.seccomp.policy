# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

@returnValue
TRAP

@headFiles
"time.h"
"sys/ioctl.h"
"linux/futex.h"
"sys/resource.h"
"sys/prctl.h"
"sys/mman.h"
"sched.h"
"fcntl.h"
"sys/random.h"
"sys/types.h"
"sys/socket.h"

@priority
futex;all

@allowList
fdatasync;all
fsync;all
ftruncate;all
getrlimit;arm64
setrlimit;all
mremap;all
pwrite64;all
sched_get_priority_max;all
sched_get_priority_min;all
getpriority;all
setpriority;all
sysinfo;all
times;all
uname;all
get_robust_list;all
set_robust_list;all
sched_getaffinity;all
sigaltstack;all
brk;all
mlock;all
munlock;all
munmap;all
mmap;arm64
sched_yield;all
nanosleep;all
epoll_pwait;all
epoll_create1;all
epoll_ctl;all
lseek;all
eventfd2;all
fstat;all
ppoll;all
pselect6;all
read;all
readv;all
pread64;all
recvfrom;all
recvmsg;all
sendmsg;all
sendto;all
write;all
writev;all
pipe2;all
gettimeofday;all
exit;all
exit_group;all
wait4;all
waitid;all
rt_sigaction;all
rt_sigprocmask;all
rt_sigreturn;all
rt_sigtimedwait;all
capget;all
getegid;all
geteuid;all
getgid;all
getgroups;all
getpid;all
getppid;all
getresgid;all
getsid;all
gettid;all
getuid;all
getresuid;all
restart_syscall;all
close;all
dup;all
dup3;all
shutdown;all
mincore;all
memfd_create;all
faccessat;all
prctl;all
fcntl;all
clone;all
setsockopt;all
kill;all
tkill;all
tgkill;all
setresuid;all
capset;all
openat;all
socket;all
connect;all
readlinkat;all
newfstatat;arm64
unlinkat;all
ioctl;all
mprotect;all
mkdirat;all
set_tid_address;all
getdents64;all
madvise;all
getrandom;all
statx;all
prlimit64;all
sched_setscheduler;all
setitimer;all
execve;all
sched_getscheduler;all
fstatfs;all
setsid;all
rt_tgsigqueueinfo;all
ptrace;all
membarrier;all
ftruncate64;arm
ugetrlimit;arm
futex_time64;arm
mmap2;arm
pause;arm
epoll_create;arm
epoll_wait;arm
eventfd;arm
fstat64;arm
_llseek;arm
poll;arm
_newselect;arm
send;arm
pipe;arm
getegid32;arm
geteuid32;arm
getgid32;arm
getgroups32;arm
getresgid32;arm
getuid32;arm
getresuid32;arm
dup2;arm
access;arm
fcntl64;arm
setresuid32;arm
open;arm
readlink;arm
unlink;arm
clock_gettime64;arm
cacheflush;arm
set_tls;arm
mkdir;arm

@allowListWithArgs
getrusage:if arg0 == RUSAGE_SELF || arg0 == RUSAGE_THREAD; return ALLOW; else return TRAP;all
clock_getres:if arg0 >= CLOCK_REALTIME && arg0 <= CLOCK_BOOTTIME; return ALLOW; else return TRAP;all
clock_gettime:if arg0 >= CLOCK_REALTIME && arg0 <= CLOCK_BOOTTIME; return ALLOW; else return TRAP;all
clock_nanosleep:if arg0 >= CLOCK_REALTIME && arg0 <= CLOCK_BOOTTIME; return ALLOW; else return TRAP;all
socketpair:if arg0 == AF_UNIX; return ALLOW; else return TRAP;all
getsockopt:if arg1 == SOL_SOCKET || arg2 == SO_PEEK_OFF; return ALLOW; else return TRAP;all
